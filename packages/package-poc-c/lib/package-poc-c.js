"use strict";

const a = require("package-poc-a");
const b = require("package-poc-b");

module.exports = packagePocC;

function packagePocC() {
  return `${a()}${b()}`;
}
